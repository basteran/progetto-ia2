require('dotenv').config();
const _ = require('lodash');
const express = require('express');
const router = express.Router();

const logger = require('../utils/logging');

// Repository è il file che si interfaccia con graphDB
const Repository = require('../repo/graphdb');


// used to resolve cors problem
router.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, x-auth-token");
    res.header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT, DELETE');
    next();
});


// find all
router.get('/', (req, res) => {
    const where = _.pick(req.body, ['sogg', 'ogg', 'pred']);

    Repository.findAll(where).then(curricula => {
        if (!curricula) return res.status(404).json({ message: 'Curricula not found.' });
        res.json(curricula);
    }).catch(err => {
        logger.error("[GET FIND ALL CURRICULA]", req.body);
        logger.error("[GET FIND ALL CURRICULA]", err);
        return res.status(500).json({ message: 'Oops, something failed!' });
    })
});

//create
router.post('/', (req, res) => {
    const where = {
        pred: ':aboutPerson',
        ogg: req.body.person
    }

    Repository.findOne(where).then(curriculum => {
        if (curriculum) return res.status(400).json({ message: 'The curriculum about ' + req.body.person + ' is already registered.' });

        curriculum = _.pick(req.body, [
            "person",
            "active",
            "confidential",
            "copyright",
            "description",
            "driven_license",
            "title",
            "courses",
            "educations",
            "skills"
        ]);

        Repository.createCV(curriculum).then(() => {
            res.json({ message: 'Succesfully registered curriculum about ' + req.body.person });
        }).catch(err => {
            logger.error("[POST NEW CURRICULUM]", req.body);
            logger.error("[POST NEW CURRICULUM]", err);
            return res.status(500).json({ message: 'Oops, something failed!' });
        })
    }).catch(err => {
        logger.error("[POST FIND ONE CURRICULUM]", req.body);
        logger.error("[POST FIND ONE CURRICULUM]", err);
        return res.status(500).json({ message: 'Oops, something failed!' });
    })
});


// find by id
router.get('/:id', (req, res) => {
    Repository.findOne({ id: req.params.id }).then(curriculum => {
        if (!curriculum) return res.status(404).json({ message: 'The curriculum with the given ID = ' + req.params.id + ' was not found.' });
        res.json(curriculum);
    }).catch(err => {
        logger.error("[GET FIND ONE CURRICULUM]", req.body);
        logger.error("[GET FIND ONE CURRICULUM]", err);
        return res.status(500).json({ message: 'Oops, something failed!' });
    })
});


module.exports = router; 