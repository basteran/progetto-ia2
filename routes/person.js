require('dotenv').config();
const _ = require('lodash');
const express = require('express');
const router = express.Router();

const logger = require('../utils/logging');

// Repository è il file che si interfaccia con graphDB
const Repository = require('../repo/graphdb');


// used to resolve cors problem
router.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, x-auth-token");
    res.header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT, DELETE');
    next();
});


//create
router.post('/', (req, res) => {
    const where = {
        sogg: req.body.id,
        pred: "a",
        ogg: ":Person"
    }

    Repository.findOne(where).then(person => {
        if (person) return res.status(400).send('The person with the given ID = ' + req.body.id + ' already registered.');

        person = _.pick(req.body, ["id", "givenName", "familyName", "birthPlace", "birthDate", "gender", "hasCitizenship", "hasDriversLicense", "noOfChildren", "maritalStatus", "hasNationality", "image"]);

        Repository.createPerson(person).then(() => {
            res.json({ message: 'Succesfully registered person with ID = ' + req.body.id });
        }).catch(err => {
            logger.error("[POST NEW PERSON]", req.body);
            logger.error("[POST NEW PERSON]", err);
            return res.status(500).json({ message: 'Oops, something failed!' });
        })
    }).catch(err => {
        logger.error("[POST FIND ONE PERSON]", req.body);
        logger.error("[POST FIND ONE PERSON]", err);
        return res.status(500).json({ message: 'Oops, something failed!' });
    })
});


// find by id
router.get('/:id', (req, res) => {
    const where = {
        'sogg': req.body.id,
        'pred': "a",
        'ogg': ":Person"
    }

    Repository.findOne(where).then(person => {
        if (!person) return res.status(404).json({ message: 'The person with the given ID = ' + req.params.id + ' was not found.' });
        res.json(person);
    }).catch(err => {
        logger.error("[GET FIND ONE PERSON]", req.body);
        logger.error("[GET FIND ONE PERSON]", err);
        return res.status(500).json({ message: 'Oops, something failed!' });
    })
});


module.exports = router; 