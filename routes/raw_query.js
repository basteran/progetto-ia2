require('dotenv').config();
const _ = require('lodash');
const express = require('express');
const router = express.Router();

const logger = require('../utils/logging');

// Repository è il file che si interfaccia con graphDB
const Repository = require('../repo/graphdb');


// used to resolve cors problem
router.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, x-auth-token");
    res.header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT, DELETE');
    next();
});


// select
router.post('/select', (req, res) => {
    const body = _.pick(req.body, ['query']);

    Repository.rawQuery(body.query).then(curricula => {
        if (!curricula) return res.status(404).send('Curricula not found.');
        res.send(curricula);
    }).catch(err => {
        logger.error("[GET FIND ALL RAW QUERY]", req.body);
        logger.error("[GET FIND ALL RAW QUERY]", err);
        return res.status(500).send('Oops, something failed!');
    })
});

//create
router.post('/', (req, res) => {

    curriculum = _.pick(req.body, ['query']);     // TODO aggiungere campi curriculum

    Repository.create(curriculum.query).then(() => {
        res.send("OK");
    }).catch(err => {
        logger.error("[POST NEW CURRICULUM]", req.body);
        logger.error("[POST NEW CURRICULUM]", err);
        return res.status(500).send('Oops, something failed!');
    })
});


module.exports = router; 