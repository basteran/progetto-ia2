const express = require('express');
const app = express();
const logger = require('./utils/logging');

require('dotenv').config();

require('./startup/routes')(app);
require('./startup/validation')();



const port = process.env.SERVER_PORT || 13000;
app.listen(port, () => logger.info(`Listening on port ${port}...`));