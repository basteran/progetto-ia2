
const error = require('../middleware/error');
const bodyParser = require('body-parser');

const curricula = require('../routes/curricula');
const raw_query = require('../routes/raw_query');
const person = require('../routes/person');

module.exports = function (app) {

    // for stripe payments
    app.use(bodyParser.json({
        verify: (req, res, buf) => {
            req.rawBody = buf
        }
    }))

    app.use('/api/curricula', curricula);
    app.use('/api/raw_query', raw_query);
    app.use('/api/person', person);


    app.use(error);

}