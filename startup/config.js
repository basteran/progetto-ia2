
require('dotenv').config();

module.exports = function() {
    if (!process.env.jwtPrivateKey){
        throw new Error("PANIC: jwtPrivateKey is not defined.");
    }
}