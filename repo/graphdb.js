const logger = require('../utils/logging');

const { ServerClient, ServerClientConfig } = require('graphdb').server;
const { RDFMimeType } = require('graphdb').http;
const { UpdateQueryPayload, GetQueryPayload, QueryType } = require('graphdb').query;
const { RepositoryClientConfig } = require('graphdb').repository;
const { SparqlJsonResultParser } = require('graphdb').parser;
const { QueryContentType } = require('graphdb').http;


const config = new ServerClientConfig('http://localhost:7200/', 0, {});
const server = new ServerClient(config);

const readTimeout = 30000;
const writeTimeout = 30000;
const repositoryClientConfig = new RepositoryClientConfig(['http://localhost:7200/repositories/cv'], {}, '', readTimeout, writeTimeout);

const prefixes = `
                    PREFIX : <http://cristoforiHromei.it/cv/data#>
                    PREFIX cv_rdfs: <http://rdfs.org/resume-rdf/cv.rdfs#> 
                    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
                    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
                    PREFIX base_rdfs: <http://rdfs.org/resume-rdf/base.rdfs#>
                    PREFIX daml: <http://www.daml.org/2001/09/countries/countries.daml#>
                    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
                `


const Repository = class Repository {

    constructor() { }


    /*
    @params
        query_type = uno tra {QueryType.SELECT, QueryType.CONSTRUCT, QueryType.DESCRIBE, QueryType.ASK}

        prefix oggetto tipo {
            nome: value, tipo rdfs
            proprieta: value, tipo p
        }
        
        sogg = soggetto, pred = predicato, ogg = oggetto
        della forma prefix:value 
            oppure 
        ?sogg ?pred ?ogg valori di default

        limit numero intero NON negativo

    */
    query(query_type = QueryType.SELECT, sogg = '?sogg', pred = '?pred', ogg = '?ogg', limit = 100000000000000) {

        // esempio query
        //     select * where {?s http://www.w3.org/2000/01/rdf-schema#label ?o}
        // 
        const payload = new GetQueryPayload()
            .setQuery(`
                ${prefixes}
                select * where {${sogg} ${pred} ${ogg}}
            `)
            .setQueryType(query_type)
            .setResponseType(RDFMimeType.SPARQL_RESULTS_JSON)
            .setLimit(limit);

        console.log(payload);
        return new Promise((resolve, reject) => {
            server.getRepository('cv', repositoryClientConfig).then((rdfRepositoryClient) => {
                rdfRepositoryClient.registerParser(new SparqlJsonResultParser());

                let data = []
                rdfRepositoryClient.query(payload).then((stream) => {
                    stream.on('data', (bindings) => {
                        data.push(bindings);
                    });
                    stream.on('end', () => {
                        console.log("no more triples");
                        if (data.length) resolve(data);
                        resolve();
                    });
                    stream.on('error', (error) => {
                        reject(error);
                    })
                }).catch(error => {
                    reject(error);
                });

            });
        });
    }

    rawQuery(raw_query, query_type = QueryType.SELECT) {
        const payload = new GetQueryPayload()
            .setQuery(raw_query)
            .setQueryType(query_type)
            .setResponseType(RDFMimeType.SPARQL_RESULTS_JSON);

        console.log(payload);
        return new Promise((resolve, reject) => {
            server.getRepository('cv', repositoryClientConfig).then((rdfRepositoryClient) => {
                rdfRepositoryClient.registerParser(new SparqlJsonResultParser());

                let data = []
                rdfRepositoryClient.query(payload).then((stream) => {
                    stream.on('data', (bindings) => {
                        data.push(bindings);
                    });
                    stream.on('end', () => {
                        console.log("no more triples");
                        if (data.length) resolve(data);
                        resolve();
                    });
                    stream.on('error', (error) => {
                        reject(error);
                    })
                });

            });
        })
    }

    findAll(where) {
        const sogg = where ? where.sogg : "?sogg";
        const pred = where ? where.pred : "?pred";
        const ogg = where ? where.ogg : "?ogg";
        return this.query(QueryType.SELECT, sogg, pred, ogg);
    }

    findOne(where) {
        const sogg = where ? where.sogg : "?sogg";
        const pred = where ? where.pred : "?pred";
        const ogg = where ? where.ogg : "?ogg";
        return this.query(QueryType.SELECT, sogg, pred, ogg, 1);
    }

    create(cv) {
        const payload = new UpdateQueryPayload()
            // .setQuery(` ${prefixes}
            //     INSERT DATA {
            //         GRAPH ${data_graph}
            //         { ${cv} }
            //     }
            // `)
            .setQuery(`${cv}`)
            // https://ontotext-ad.github.io/graphdb.js/global.html#QueryContentType
            .setContentType(QueryContentType.X_WWW_FORM_URLENCODED)
            .setInference(true)
            .setTimeout(5);

        console.log(payload);
        return new Promise((resolve, reject) => {
            server.getRepository('cv', repositoryClientConfig).then((rdfRepositoryClient) => {
                rdfRepositoryClient.registerParser(new SparqlJsonResultParser());

                rdfRepositoryClient.update(payload).then(() => {
                    resolve();
                }).catch(error => {
                    reject(error);
                });

            });
        })

    }


    createPerson(person) {
        const payload = new UpdateQueryPayload()
            .setQuery(`
            ${prefixes}

            INSERT DATA {
                GRAPH <http://cristoforiHromei.it/cv/data> {
                    ${person.id} a :Person;
                        :gender ${person.gender};
                        :givenName "${person.givenName}"^^xsd:string;
                        :familyName "${person.familyName}"^^xsd:string;
                        :birthPlace "${person.birthPlace}"^^xsd:string;
                        :birthDate "${person.birthDate}"^^xsd:string;
                        :hasCitizenship ${person.hasCitizenship};
                        :hasNationality "${person.hasNationality}"^^xsd:string;
                        :maritalStatus ${person.maritalStatus};
                        :noOfChildren "${person.noOfChildren}"^^xsd:integer;
                        :hasDriversLicense "${person.hasDriversLicense}"^^xsd:boolean;
                        :image "${person.image}"^^xsd:string.
                }
            }
        `)
            // https://ontotext-ad.github.io/graphdb.js/global.html#QueryContentType
            .setContentType(QueryContentType.X_WWW_FORM_URLENCODED)
            .setInference(true)
            .setTimeout(5);

        console.log(payload);

        return new Promise((resolve, reject) => {
            server.getRepository('cv', repositoryClientConfig).then((rdfRepositoryClient) => {
                rdfRepositoryClient.registerParser(new SparqlJsonResultParser());

                rdfRepositoryClient.update(payload).then(() => {
                    resolve();
                }).catch(error => {
                    reject(error);
                });

            });
        })
    }



    createCV(cv) {

        this.createCourses(cv.courses, `${cv.person}_CV`);
        this.createEducations(cv.educations, `${cv.person}_CV`);
        this.createSkills(cv.skills, `${cv.person}_CV`);

        const today = (new Date()).toLocaleDateString();
        const payload = new UpdateQueryPayload()
            .setQuery(`
                ${prefixes}

                INSERT DATA {
                    GRAPH <http://cristoforiHromei.it/cv/data> {
                        ${cv.person}_CV a :CV;
                                    :aboutPerson ${cv.person};
                                    :cvCopyright "${cv.copyright}"^^xsd:boolean;
                                    :cvDescription "${cv.description}"^^xsd:string;
                                    :cvIsActive "${cv.active}"^^xsd:boolean;
                                    :cvIsConfidential "${cv.confidential}"^^xsd:boolean;
                                    :cvTitle "${cv.title}"^^xsd:string;
                                    :lastUpdate "${today}"^^xsd:string .
                    }
                }
            `)
            // https://ontotext-ad.github.io/graphdb.js/global.html#QueryContentType
            .setContentType(QueryContentType.X_WWW_FORM_URLENCODED)
            .setInference(true)
            .setTimeout(5);

        console.log(payload);

        return new Promise((resolve, reject) => {
            server.getRepository('cv', repositoryClientConfig).then((rdfRepositoryClient) => {
                rdfRepositoryClient.registerParser(new SparqlJsonResultParser());

                rdfRepositoryClient.update(payload).then(() => {
                    resolve();
                }).catch(error => {
                    reject(error);
                });

            });
        })
    }


    createCourses(courses, cv) {
        let payload;

        courses.forEach(course => {
            this.findOne({
                sogg: course.id,
                pred: 'a',
                ogg: ':Course'
            }).then(existingCourse => {
                if (!existingCourse) {
                    payload = new UpdateQueryPayload()
                        .setQuery(`
                            ${prefixes}

                            INSERT DATA {
                                GRAPH <http://cristoforiHromei.it/cv/data> {
                                    ${cv} :hasCourse ${course.id}.

                                    ${course.id} a :Course;
                                            :courseDescription "${course.courseDescription}"^^xsd:string;
                                            :courseFinishDate "${course.courseFinishDate}"^^xsd:string;
                                            :courseStartDate "${course.courseStartDate}"^^xsd:string;
                                            :courseTitle "${course.courseTitle}"^^xsd:string;
                                            :courseURL "${course.courseURL}"^^xsd:string;
                                            :isCertification "${course.isCertification}"^^xsd:boolean;
                                            :organizedBy ${course.organizedBy}.
                                }
                            }
                        `)
                        // https://ontotext-ad.github.io/graphdb.js/global.html#QueryContentType
                        .setContentType(QueryContentType.X_WWW_FORM_URLENCODED)
                        .setInference(true)
                        .setTimeout(5);

                    console.log(payload);
                    server.getRepository('cv', repositoryClientConfig).then((rdfRepositoryClient) => {
                        rdfRepositoryClient.registerParser(new SparqlJsonResultParser());
                        rdfRepositoryClient.update(payload);
                    });
                } else {
                    payload = new UpdateQueryPayload()
                        .setQuery(`
                            ${prefixes}

                            INSERT DATA {
                                GRAPH <http://cristoforiHromei.it/cv/data> {
                                    ${cv} :hasCourse ${course.id}.
                                }
                            }
                        `)
                        // https://ontotext-ad.github.io/graphdb.js/global.html#QueryContentType
                        .setContentType(QueryContentType.X_WWW_FORM_URLENCODED)
                        .setInference(true)
                        .setTimeout(5);

                    console.log(payload);
                    server.getRepository('cv', repositoryClientConfig).then((rdfRepositoryClient) => {
                        rdfRepositoryClient.registerParser(new SparqlJsonResultParser());
                        rdfRepositoryClient.update(payload);
                    });
                }
            })
        })

    }


    createEducations(educations, cv) {
        let payload;

        educations.forEach(education => {
            this.findOne({
                sogg: education.id,
                pred: 'a',
                ogg: ':Education'
            }).then(existingEducation => {
                if (!existingEducation) {
                    payload = new UpdateQueryPayload()
                        .setQuery(`
                            ${prefixes}

                            INSERT DATA {
                                GRAPH <http://cristoforiHromei.it/cv/data> {
                                    ${cv} :hasEducation ${education.id}.

                                    ${education.id} a :Education;
                                            :degreeType "${education.degreeType}"^^xsd:string;
                                            :eduDescription "${education.eduDescription}"^^xsd:string;
                                            :eduGradDate "${education.eduGradDate}"^^xsd:string;
                                            :eduMajor "${education.eduMajor}"^^xsd:string;
                                            :eduMinor "${education.eduMinor}"^^xsd:string;
                                            :eduStartDate "${education.eduStartDate}"^^xsd:string;
                                            :studiedIn ${education.studiedIn}.
                                }
                            }
                        `)
                        // https://ontotext-ad.github.io/graphdb.js/global.html#QueryContentType
                        .setContentType(QueryContentType.X_WWW_FORM_URLENCODED)
                        .setInference(true)
                        .setTimeout(5);

                    console.log(payload);
                    server.getRepository('cv', repositoryClientConfig).then((rdfRepositoryClient) => {
                        rdfRepositoryClient.registerParser(new SparqlJsonResultParser());
                        rdfRepositoryClient.update(payload);
                    });
                } else {
                    payload = new UpdateQueryPayload()
                        .setQuery(`
                            ${prefixes}

                            INSERT DATA {
                                GRAPH <http://cristoforiHromei.it/cv/data> {
                                    ${cv} :hasEducation ${education.id}.
                                }
                            }
                        `)
                        // https://ontotext-ad.github.io/graphdb.js/global.html#QueryContentType
                        .setContentType(QueryContentType.X_WWW_FORM_URLENCODED)
                        .setInference(true)
                        .setTimeout(5);

                    console.log(payload);
                    server.getRepository('cv', repositoryClientConfig).then((rdfRepositoryClient) => {
                        rdfRepositoryClient.registerParser(new SparqlJsonResultParser());
                        rdfRepositoryClient.update(payload);
                    });
                }
            })
        })

    }

    createSkills(skills, cv) {
        let payload;

        skills.forEach(skill => {
            this.findOne({
                sogg: skill.id,
                pred: 'a',
                ogg: ':Skill'
            }).then(existingSkill => {
                if (!existingSkill) {
                    payload = new UpdateQueryPayload()
                        .setQuery(`
                            ${prefixes}

                            INSERT DATA {
                                GRAPH <http://cristoforiHromei.it/cv/data> {
                                    ${cv} :hasSkill ${skill.id}.

                                    ${skill.id} a :Skill;
                                            a ${skill.skillType};
                                            :skillLastUsed "${skill.skillLastUsed}"^^xsd:string;
                                            :skillLevel "${skill.skillLevel}"^^xsd:string;
                                            :skillName "${skill.skillName}"^^xsd:string;
                                            :skillYearsExperience "${skill.skillYearsExperience}"^^xsd:string.
                                }
                            }
                        `)
                        // https://ontotext-ad.github.io/graphdb.js/global.html#QueryContentType
                        .setContentType(QueryContentType.X_WWW_FORM_URLENCODED)
                        .setInference(true)
                        .setTimeout(5);

                    console.log(payload);
                    server.getRepository('cv', repositoryClientConfig).then((rdfRepositoryClient) => {
                        rdfRepositoryClient.registerParser(new SparqlJsonResultParser());
                        rdfRepositoryClient.update(payload);
                    });
                } else {
                    payload = new UpdateQueryPayload()
                        .setQuery(`
                            ${prefixes}

                            INSERT DATA {
                                GRAPH <http://cristoforiHromei.it/cv/data> {
                                    ${cv} :hasSkill ${skill.id}.
                                }
                            }
                        `)
                        // https://ontotext-ad.github.io/graphdb.js/global.html#QueryContentType
                        .setContentType(QueryContentType.X_WWW_FORM_URLENCODED)
                        .setInference(true)
                        .setTimeout(5);

                    console.log(payload);
                    server.getRepository('cv', repositoryClientConfig).then((rdfRepositoryClient) => {
                        rdfRepositoryClient.registerParser(new SparqlJsonResultParser());
                        rdfRepositoryClient.update(payload);
                    });
                }
            })
        })

    }
}
module.exports = new Repository();