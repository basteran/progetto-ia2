require('dotenv').config();

const winston = require('winston');
const { format } = require('logform');


const transportsLog = new winston.transports.Console({
    timestamp: () => moment().format('DD/MM/YYYY h:mm:ss:SSS'),
    colorize: true,
    format: format.combine(
        format.colorize(),
        format.timestamp(),
        format.splat(),
        format.simple(),
    ),
})


const logger = new winston.createLogger({
    level: process.env.LOG_LEVEL ? process.env.LOG_LEVEL : 'silly',
    format: winston.format.json(),
    exitOnError: false,
    transports: [
        transportsLog,
        new winston.transports.File({ filename: 'logfile.log' })
    ]
});

module.exports = logger;
