
// return a function "model" that executes the code in the try catch block
module.exports = function (handler){
    return async (req,res,next) =>{
      try{
        await handler();
      } catch(ex) {
        next(ex)
      }
    };
}