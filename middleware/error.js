
const logger = require('../utils/logging');

// middleware function for Logging the errors
module.exports = function(err, req, res, next){
    logger.error(err.message, err);

    res.status(500).send('Something failed.');
    // console.log(ex);
}